use structopt::StructOpt;
use std::io::BufReader;
use std::fs::File;

/// search for a pattern in a file and display the lines that contain it
#[derive(StructOpt)]
struct Cli {
    /// the patter to look for
    pattern: String,
    /// the path to the file to read
    #[structopt(parse(from_os_str))]
    path: std::path::PathBuf,
}
fn main() {
    let args = Cli::from_args();
    let f = File::open(&args.path);
    let reader = BufReader::new(f);
    // let content = std::fs::read_to_string(&args.path)
    //     .expect("could not read file");

    for line in reader.lines() {
        if line.contains(&args.pattern) {
            println!("{}", line);
        }
    }
    // print CLI arguments to screen (just an exercise)
    // println!("{}", args.pattern);
    // println!("{:?}", args.path);
}
